import torch
import torchvision.models as models
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import numpy as np
import matplotlib.pyplot as plt
import argparse
import os
import json
import csv
from models import pointnet2_cls_ssg as pcsmodel
import open3d as o3d
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
import time
import fastprogress
import random
import copy

##################################
## Argument Parser
##################################

parser = argparse.ArgumentParser()
parser.add_argument('--dataset',
                    help='''CSV file containing absolute point 
                    cloud paths or relative to this file''')
parser.add_argument('--weight',
                    help='''Trained model weights/
                    Directory having several weights''')

parser.add_argument('--data_split',
                    help='''JSON file containing data split, indices in accordance to
                    dataset CSV File.''')

parser.add_argument('--normals',
                    action='store_true',
                    help=''' Consider normals while predicting
                      ''')


args = parser.parse_args()

if not args.dataset or not args.weight or not args.data_split:
    print("One of the attribute is missing")
    exit(0)

##################################
## Variables & Helper Functions
##################################
csv_file = args.dataset
weight_path = args.weight
data_split = args.data_split
_normals = True if args.normals else False


class PCDataset(Dataset):
    def __init__(self, csv_path):
        self.csv = csv_path
        self.pcds = []
        self.labels = []
        
        with open(csv_path, mode ='r')as file:
            csvreader = csv.reader(file)
            for line in csvreader:
                self.pcds.append(line[0])
                self.labels.append(line[1])
    
    def __len__(self):
        return len(self.pcds)

    def __getitem__(self, index):
        pcd = o3d.io.read_point_cloud(self.pcds[index])
        if _normals:
            pcd.estimate_normals()
            pcd = np.concatenate( (np.asarray(pcd.points), np.asarray(pcd.normals)), axis = 1 )
        else:
            pcd = np.asarray(pcd.points)
        return torch.from_numpy(pcd).to(torch.float32), torch.tensor([int(self.labels[index])])

def get_test_samples(path):
    test_samples = []
    with open(path) as f:
        data = json.load(f)
        indices = data["indices"]
        test_index = int(data["test_indices"])
        train_index = int(data["train_indices"])
        for j in range(train_index+1):
            test_samples.append(int(indices[j]))
    return test_samples

def load_model_weights(path, num_classes, _normals):
    model = pcsmodel.get_model(num_class = num_classes, normal_channel = _normals)
    model.load_state_dict(torch.load(path))
    
    return model

def get_point_clouds(path,indices):
    with open(path, newline = "") as file:
        csvreader = csv.reader(file)
        return [row for idx, row in enumerate(csvreader) if idx in set(indices)]


def get_test_results(model, data, label):
    _, y_pred = model(data)

    print(y_pred.shape)
    res = y_pred.argmax(dim=1)
    print(res.shape)
    label = torch.tensor([int(label)])
    return sum(label.to(device) == res)

def downsample_tensor(pcd ,num_pts):
        indices = torch.randperm(pcd.shape[0])[:num_pts]
        pcd = torch.index_select(pcd, 0, indices)
        return pcd

def custom_collate_func(batch):
    
    point_clouds=  [i[0] for i in batch]
    labels = torch.tensor([i[1] for i in batch])
    downsample_size= 100_000 
    point_clouds = torch.tensor([downsample_tensor(i, downsample_size).tolist() for i in point_clouds])
    return [point_clouds, labels]

def get_test_loader(csv_file, indices, batch_size=4):
    dataset = PCDataset(csv_path = csv_file)
    testloader = torch.utils.data.DataLoader(torch.utils.data.Subset(dataset, indices), batch_size=batch_size,
                                            collate_fn=custom_collate_func, shuffle=True)
    
    return testloader

def accuracy(correct, total): 
    return float(correct)/total

def test(dataloader, model, loss_fn, device, master_bar):
    epoch_loss = []
    epoch_correct, epoch_total = 0, 0
    confusion_matrix = torch.zeros(10, 10)    

    #model.eval()
    with torch.no_grad():
        for x, y in fastprogress.progress_bar(dataloader, parent=master_bar):
            
            x = x.transpose(2, 1)
            _, y_pred = model(x.to(device))
            y = y.view(len(y), 1)
            epoch_correct += sum(y.to(device) == y_pred.argmax(dim=1))
            epoch_total += len(y)

            loss = loss_fn(y_pred, y.to(device))

            epoch_loss.append(loss.item())
    return np.mean(epoch_loss), accuracy(epoch_correct, epoch_total)

def run_testing(model, loss_function, device, 
                test_dataloader):
    start_time = time.time()
    master_bar = fastprogress.master_bar(range(1))

    epoch_test_loss, epoch_test_acc = test(test_dataloader, 
                                                model, loss_function, 
                                                device, master_bar)

        
    time_elapsed = np.round(time.time() - start_time, 0).astype(int)
    print(f'Finished testing after {time_elapsed} seconds.')
    return epoch_test_loss, epoch_test_acc
    

def downsample_tensor(pcd ,num_pts):
    indices = torch.randperm(pcd.shape[0])[:num_pts]
    pcd = torch.index_select(pcd, 0, indices)
    
    return pcd

def compare_models(model1, model2):
    for p1, p2 in zip(model1.parameters(), model2.parameters()):
        if p1.data.ne(p2.data).sum() > 0:
            return False
    return True

##################################
## Driver Function
##################################
if __name__ == "__main__":
    
    test_indices = get_test_samples(data_split)
    point_clouds = get_point_clouds(csv_file, test_indices)

    testloader = get_test_loader(csv_file, test_indices, batch_size=4)
    model = pcsmodel.get_model(num_class = 12, normal_channel = _normals)
    model_copy = copy.deepcopy(model)
    if not os.path.isdir(weight_path):
        model.load_state_dict(torch.load(weight_path))
        print(compare_models(model_copy, model))
        model.eval()
        device = 'cpu'#torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
        model.to(device)
        
        criterion = nn.CrossEntropyLoss()
       
        test_loss, test_acc = run_testing(model, criterion, device,
                    testloader)
        print(test_loss, test_acc)

    else:
        weights = sorted(os.listdir(weight_path))
        test_accs = []
        test_losses = []
        for weight in weights:
            path = os.path.join(weight_path, weight)
            model.load_state_dict(torch.load(path))
            print(weight, compare_models(model_copy, model))
            model.eval()
            device = 'cpu'#torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
            model.to(device)
            
            criterion = nn.CrossEntropyLoss()
            
            num_epochs = 1
            test_loss, test_acc = run_testing(model, criterion, device, num_epochs, 
                        testloader)
            test_losses.append(test_loss)
            test_accs.append(test_acc)
            print(test_loss, test_acc)
        
        print(test_losses)
        print(test_accs)
        plt.plot([i for i in range(len(test_accs))], test_accs)
        plt.show()
