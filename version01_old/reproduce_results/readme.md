## Usage instructions

### Execution

```python
python3 intermediate_results.py --dataset [csv_file] --weight [model_weight(s)] --data_split [JSON_file] (--normals)
```

### Arguments Description
1. <b>Dataset</b> (required): ```.csv``` file containing path of point clouds and a numeric value assigned to their class.
2. <b>Weight</b> (required): Either ```.pth``` or ```.pth.tar``` file containing weights for the model to be loaded or a directory containing several weight files. 
3. <b>Data_split</b> (required): ```.json``` file containing a random permutation of indices of dataset, starting index of training samples and test samples.
4. <b>Normals</b> (optional): If normals should be used while testing.
