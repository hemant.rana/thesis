
import os
import sys
import torch
import numpy as np

import datetime
import logging
import provider
import importlib
import shutil
import argparse

from pathlib import Path
from tqdm import tqdm
from data_utils.ModelNetDataLoader import ModelNetDataLoader
from data_utils.PCDataLoaderRegression import PCDatasetRegression, generate_csv, custom_collate_func
# from models.update_regression import get_updated_regression_model
# from data_utils.PCDataLoaderOcclusion import (
#                                             PCDataset as PCDatasetOccluded,
#                                             custom_collate_func as custom_collate_func_occluded)

import matplotlib.pyplot as plt

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = BASE_DIR
sys.path.append(os.path.join(ROOT_DIR, 'models'))

def parse_args():
    '''PARAMETERS'''
    parser = argparse.ArgumentParser('training')
    parser.add_argument('--use_cpu', action='store_true', default=False, help='use cpu mode')
    parser.add_argument('--gpu', type=str, default='0', help='specify gpu device')
    parser.add_argument('--batch_size', type=int, default=4, help='batch size in training')
    parser.add_argument('--model', default='pointnet2_reg_msg', help='model name [default: pointnet_cls]')
    parser.add_argument('--num_category', default=12, type=int,  help='training on Custom Data')
    parser.add_argument('--epoch', default=20, type=int, help='number of epoch in training')
    parser.add_argument('--learning_rate', default=0.001, type=float, help='learning rate in training')
    parser.add_argument('--num_point', type=int, default=1024, help='Point Number')
    parser.add_argument('--optimizer', type=str, default='Adam', help='optimizer for training')
    parser.add_argument('--log_dir', type=str, default=None, help='experiment root')
    parser.add_argument('--decay_rate', type=float, default=1e-4, help='decay rate')
    parser.add_argument('--use_normals', action='store_true', default=False, help='use normals')
    parser.add_argument('--process_data', action='store_true', default=False, help='save data offline')
    parser.add_argument('--use_uniform_sample', action='store_true', default=False, help='use uniform sampiling')
    parser.add_argument('--generate_data', action='store_true', default=False, 
                        help='Generate CSV Files for train/test/val datasets')
    parser.add_argument('--occlude', action='store_true', default=False, 
                        help='Whether to train with occluded point clouds')
                        
    parser.add_argument('--data_dir', 
                        help='Dataset directory with point clouds')
    parser.add_argument('--data_split', default=None, 
                        help='Directory with data split CSVs')
    return parser.parse_args()


def inplace_relu(m):
    classname = m.__class__.__name__
    if classname.find('ReLU') != -1:
        m.inplace=True


def test(model, loader, criterion = torch.nn.MSELoss(), num_class=12):
    mean_correct = []
    class_acc = np.zeros((num_class, 3))
    classifier = model.eval()

    for j, (points, target) in tqdm(enumerate(loader), total=len(loader)):

        if not args.use_cpu:
            points, target = points.cuda(), target.cuda()

        points = points.transpose(2, 1)
        pred, _ = classifier(points)
        # pred_choice = pred.data.max(1)[1]

        # for cat in np.unique(target.cpu()):
        #     classacc = pred_choice[target == cat].eq(target[target == cat].long().data).cpu().sum()
        #     class_acc[cat, 0] += classacc.item() / float(points[target == cat].size()[0])
        #     class_acc[cat, 1] += 1

        # correct = pred_choice.eq(target.long().data).cpu().sum()
        mean_correct.append(criterion(pred, target).item())

    # class_acc[:, 2] = class_acc[:, 0] / class_acc[:, 1]
    # class_acc = np.mean(class_acc[:, 2])
    instance_acc = np.mean(mean_correct)

    return instance_acc


def main(args):
    def log_string(str):
        logger.info(str)
        print(str)

    # print("cuda specs", torch.cuda.is_available(), torch.cuda.current_device())

    '''HYPER PARAMETER'''
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu

    '''CREATE DIR'''
    timestr = str(datetime.datetime.now().strftime('%Y-%m-%d_%H-%M'))
    exp_dir = Path('./log/')
    exp_dir.mkdir(exist_ok=True)
    exp_dir = exp_dir.joinpath('regression')
    exp_dir.mkdir(exist_ok=True)
    if args.log_dir is None:
        exp_dir = exp_dir.joinpath(timestr)
    else:
        exp_dir = exp_dir.joinpath(args.log_dir)
    exp_dir.mkdir(exist_ok=True)
    checkpoints_dir = exp_dir.joinpath('checkpoints/')
    checkpoints_dir.mkdir(exist_ok=True)
    log_dir = exp_dir.joinpath('logs/')
    log_dir.mkdir(exist_ok=True)

    '''LOG'''
    args = parse_args()
    logger = logging.getLogger("Model")
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    file_handler = logging.FileHandler('%s/%s.txt' % (log_dir, args.model))
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    log_string('PARAMETER ...')
    log_string(args)

    '''Generating CSV's'''
    if args.generate_data:
        dataset_path = args.data_split
        #if not os.path.exists(args.data_split):
        #   os.mkdir(args.data_split)
        generate_csv(
        	os.path.join(dataset_path, 'regression_parameters.csv'),
        	dataset_path)
        	
        print('Done generating data')

    '''DATA LOADING'''
    log_string('Load dataset ...')
    data_path = 'data/modelnet40_normal_resampled/'

    if True:#not args.occlude:
        print("Without Occlusion")
        train_dataset = PCDatasetRegression(split = "train", path = args.data_split, normals = args.use_normals)
        test_dataset = PCDatasetRegression(split = "val", path = args.data_split, normals = args.use_normals)
        trainDataLoader = torch.utils.data.DataLoader(train_dataset, batch_size=args.batch_size, shuffle=True, collate_fn=custom_collate_func, num_workers = 4)
        testDataLoader = torch.utils.data.DataLoader(test_dataset, batch_size=args.batch_size, shuffle=False,collate_fn=custom_collate_func, num_workers = 4)
    # else:
    #     print("Using Occlusion")
    #     train_dataset = PCDatasetOccluded(split = "train", path = "./dataloader_output", normals = args.use_normals)
    #     test_dataset = PCDatasetOccluded(split = "val", path = "./dataloader_output", normals = args.use_normals)
    #     trainDataLoader = torch.utils.data.DataLoader(train_dataset, batch_size=args.batch_size, shuffle=True, num_workers=1, collate_fn=custom_collate_func_occluded)
    #     testDataLoader = torch.utils.data.DataLoader(test_dataset, batch_size=args.batch_size, shuffle=False, num_workers=1,collate_fn=custom_collate_func_occluded)
    
    # train_dataset = ModelNetDataLoader(root=data_path, args=args, split='train', process_data=args.process_data)
    # test_dataset = ModelNetDataLoader(root=data_path, args=args, split='test', process_data=args.process_data)
    # trainDataLoader = torch.utils.data.DataLoader(train_dataset, batch_size=args.batch_size, shuffle=True, num_workers=2, drop_last=True)
    # testDataLoader = torch.utils.data.DataLoader(test_dataset, batch_size=args.batch_size, shuffle=False, num_workers=2)

    # train_features, train_labels = next(iter(trainDataLoader))
    # print(train_features.size(), train_labels.size())
    # print(train_features, train_labels)

    '''MODEL LOADING'''
    num_class = args.num_category
    model = importlib.import_module(args.model)
    shutil.copy('./models/%s.py' % args.model, str(exp_dir))
    shutil.copy('models/pointnet2_utils.py', str(exp_dir))
    shutil.copy('./train_regression.py', str(exp_dir))

    classifier = model.get_model(path = "./log/classification/2023-07-22_22-33/checkpoints/best_model.pth",
                                 update = True)
    # criterion = model.get_loss()
    criterion = torch.nn.MSELoss()
    # print(criterion)
    classifier.apply(inplace_relu)

    if not args.use_cpu:
        classifier = classifier.cuda()
        criterion = criterion.cuda()

    ##############################
    ## Checking best instance acc from checkpoint
    #############################
    best_instance_acc = float('inf')


    try:
        checkpoint = torch.load(str(exp_dir) + '/checkpoints/best_model.pth')
        start_epoch = checkpoint['epoch']
        classifier.load_state_dict(checkpoint['model_state_dict'])
        best_instance_acc = checkpoint['instance_acc']
        log_string('Use pretrain model')
    except:
        log_string('No existing model, starting training from scratch...')
        start_epoch = 0

    if args.optimizer == 'Adam':
        optimizer = torch.optim.Adam(
            classifier.parameters(),
            lr=args.learning_rate,
            betas=(0.9, 0.999),
            eps=1e-08,
            weight_decay=args.decay_rate
        )
    else:
        optimizer = torch.optim.SGD(classifier.parameters(), lr=0.01, momentum=0.9, weight_decay = args.decay_rate)

    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=20, gamma=0.7)
    global_epoch = 0
    global_step = 0
    best_class_acc = 0.0
    train_score = [] 
    val_score =[]  
    class_scores =[]
    '''TRANING'''
    logger.info('Start training...')
    for epoch in range(start_epoch, args.epoch):
        log_string('Epoch %d (%d/%s):' % (global_epoch + 1, epoch + 1, args.epoch))
        mean_correct = []
        classifier = classifier.train()
         
        scheduler.step()
        for batch_id, (points, target) in tqdm(enumerate(trainDataLoader, 0), total=len(trainDataLoader), smoothing=0.9):
            optimizer.zero_grad()

            points = points.data.numpy()
            points = provider.random_point_dropout(points)
            points[:, :, 0:3] = provider.random_scale_point_cloud(points[:, :, 0:3])
            points[:, :, 0:3] = provider.shift_point_cloud(points[:, :, 0:3])
            points = torch.Tensor(points)
            # print(points.size())
            points = points.transpose(2, 1)
            # print(points.shape, target)

            if not args.use_cpu:
                points, target = points.cuda(), target.cuda()

            # print(batch_id, points.shape)
            pred, trans_feat = classifier(points)
            # loss_test = torch.nn.MSELoss()
            # print(loss_test(target, pred))
            # print(target.shape, pred.shape) # torch.Size([2, 1, 4]) torch.Size([2, 4])
            # print(pred.shape,target, target-1,  trans_feat.shape, pred)
            # loss = criterion(pred, target, trans_feat)
            loss = criterion(pred, target)
            # pred_choice = pred.data.max(1)[1]

            # print("loss ====> ", loss)

            # correct = pred_choice.eq(target.long().data).cpu().sum()
            mean_correct.append(loss.item())
            loss.backward()
            optimizer.step()
            global_step += 1

        train_instance_acc = np.mean(mean_correct)
        train_score.append(train_instance_acc)
        log_string('Train Instance Accuracy: %f' % train_instance_acc)

        #SAving model
        torch.save(classifier.state_dict(), "./savepoints/epoch_{}.pth.tar".format(epoch))

        with torch.no_grad():
            instance_acc = test(classifier.eval(), testDataLoader, num_class=num_class)

            if (instance_acc <= best_instance_acc):
                best_instance_acc = instance_acc
                best_epoch = epoch + 1

            # if (class_acc >= best_class_acc):
            #     best_class_acc = class_acc
            log_string('Test Instance Accuracy: %f' % (instance_acc))
            log_string('Best Instance Accuracy: %f' % (best_instance_acc))

            if (instance_acc <= best_instance_acc):
                logger.info('Save model...')
                savepath = str(checkpoints_dir) + '/best_model.pth'
                log_string('Saving at %s' % savepath)
                state = {
                    'epoch': best_epoch,
                    'instance_acc': instance_acc,
                    # 'class_acc': class_acc,
                    'model_state_dict': classifier.state_dict(),
                    'optimizer_state_dict': optimizer.state_dict(),
                }
                torch.save(state, savepath)
            global_epoch += 1
        # val_score.append(instance_acc)
        # class_scores.append(class_acc)

    # print(train_score, val_score, class_scores)

    # plt.plot([i for i in range(len(train_score))],train_score, label = 'Training')
    # plt.plot([i for i in range(len(val_score))],val_score, label = 'Validation')
    # plt.legend()
    # plt.show()
    logger.info('End of training...')



if __name__ == '__main__':
    args = parse_args()
    main(args)
