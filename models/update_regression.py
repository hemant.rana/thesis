import torch
import numpy as np
from pointnet2_reg_msg import get_model as reg_model
from pointnet2_cls_msg import get_model as cls_model
import torch.nn as nn
import sys
import copy

sys.path.insert(1, '../models')
sys.path.insert(2, '../')

def get_updated_regression_model(num_classes = 12,
                                 path = None,
                                 num_reg_params = 3):
    
    regression_model = reg_model()
    classification_model = cls_model(num_classes, False)

    classification_model.load_state_dict(torch.load(path)["model_state_dict"])

    #####################
    ## Copying weights
    #####################
    with torch.no_grad():
        
        regression_model.sa1.load_state_dict(
            classification_model.sa1.state_dict()
        )
        
        regression_model.sa2.load_state_dict(
            classification_model.sa2.state_dict()
        )

        regression_model.sa3.load_state_dict(
            classification_model.sa3.state_dict()
        )

    return regression_model

#####################
## For testing 
#####################
if __name__ == "__main__":
    model = get_updated_regression_model(path = "/mnt/d/thesis/Pointnet_Pointnet2_pytorch/log/classification/2023-07-22_22-33/checkpoints/best_model.pth")
    

