import open3d as o3d
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import torch
import argparse

sys.path.insert(1, '../models')
sys.path.insert(2, '../')

from models.pointnet2_cls_msg import get_model as model_msg
# import models.pointnet2_cls_msg
# from models.pointnet2_cls_ssg import get_model as model_ssg
import provider

################################################
## Argument Parser
################################################

parser = argparse.ArgumentParser()
parser.add_argument('--log_dir',
                    help='''Directory containing log file''',
                    default = None)

parser.add_argument('--model',
                    help='''Model Name for saving outputs | options: ssg, msg''',
                    default = "msg")

parser.add_argument('--file',
                    help = ''' PCD File for processing ''')

parser.add_argument('--voxel',
                    help = '''Voxel downsample size''',
                    default = None)

parser.add_argument('--use_normals',
                    help = '''Whether to use normals or not''')

args = parser.parse_args()

if not args.file:
    print("No Point Cloud file mentioned")
    exit(0)

################################################
## Initial Variables
################################################
pcd_path = args.file
output_path = "_".join([
                pcd_path.split("/")[-1].split(".")[0],
                args.model,
                "0" if not args.voxel else args.voxel
                ])

# print(output_path)

if not os.path.exists(output_path):
    os.mkdir(output_path)

if args.model == "ssg":
    model = model_ssg(12, True if args.use_normals else False)
else:
    model = model_msg(12, True if args.use_normals else False)

if args.log_dir:
    weight_path = "../log/classification/{}/checkpoints/best_model.pth".format(args.log_dir)
    model.load_state_dict(torch.load(weight_path)["model_state_dict"])

################################################
## Helper Functions
################################################

def process_points(points):
    points = points.data.numpy()
    points = provider.random_point_dropout(points)
    points[:, :, 0:3] = provider.random_scale_point_cloud(points[:, :, 0:3])
    points[:, :, 0:3] = provider.shift_point_cloud(points[:, :, 0:3])
    points = torch.Tensor(points)
    points = points.transpose(2, 1)
    return points

def tensor_to_point_cloud(points):
    selected_points = points[0].transpose(1, 0).detach().cpu().numpy()
    intermediate_pcd = o3d.geometry.PointCloud()
    intermediate_pcd.points = o3d.utility.Vector3dVector(selected_points)
    return intermediate_pcd


################################################
## Driver Program
################################################

if __name__ == '__main__':
    pcd = o3d.io.read_point_cloud(pcd_path)
    if args.voxel:
        pcd = pcd.voxel_down_sample(float(args.voxel))
        o3d.io.write_point_cloud(os.path.join(output_path, "0_.pcd")
        , pcd)

    points_tensor = torch.from_numpy(
                    np.asarray([np.asarray(pcd.points)])).to(torch.float32)
    points_tensor = process_points(points_tensor)
    a = points_tensor
    b = None
    for idx, (name, module) in enumerate(model.named_children()):
        if "PointNetSetAbstraction" in str(module):
            a, b = module(a, b)
            new_pcd = tensor_to_point_cloud(a)
            o3d.io.write_point_cloud(os.path.join(output_path,
                "layer_{}.pcd".format(idx+1)), new_pcd)
        else:
            break
