import os
import torch
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset
import open3d as o3d
import csv
import torch
import open3d as o3d
import random
import warnings
import argparse
warnings.filterwarnings("ignore")
import random
plt.ion() 

##################################
## Argument Parser
##################################

# parser = argparse.ArgumentParser()
# parser.add_argument('--dataset',
#                     help='''Directory containing Point Clouds''')
# parser.add_argument('--output',
#                     help='''Where CSV file and splitting indices are saved''')

# args = parser.parse_args()

# if not args.dataset or not args.output:
#     print("One of the attribute is missing")
#     exit(0)

# dataset_path = args.dataset
# output_path = args.output

####################################
### Helper Functions
####################################

def downsample_tensor(pcd, point_label ,num_pts, label):
    # print("in downsample function", pcd.shape, point_label.shape, label, point_label + ((label)*25), torch.unique(point_label + ((label)*25))  )
    indices = torch.randperm(pcd.shape[0])[:num_pts]
    pcd = torch.index_select(pcd, 0, indices)
    point_label = torch.index_select(point_label, 0, indices)
    point_label = point_label + ((label)*25)
    return pcd, point_label

def write_csv_file(save_path, data):
    with open(save_path, 'w+', newline = '') as file:
        writer = csv.writer(file)
        for row in data: writer.writerow(row)

def get_mappings():
    mappings = {}
    for i in range(1,13):
        mappings[i] = i
    tags = [11,4,1,9,7,5,10,3,2,6,8,
            12,7,4,2,1,5,9,11,3,10,12,
            8,6,5,8,7,6,1,12,2,9,11,3,10,4]

    for i in range(len(tags)):
        mappings[i+13] = tags[i] 
    return mappings

def split_data(class_dict, test_ratio = 0.2, val_ratio = 0.2):

    train_samples = []
    test_samples = []
    val_samples = []

    for class_label in class_dict:
        random.shuffle(class_dict[class_label]) 
        n = len(class_dict[1])
        train_index = int(n * (1.0-test_ratio-val_ratio)) 
        test_index = train_index + int(n*test_ratio)

        if test_index == train_index:
            diff = (n - train_index)//2
            test_index = train_index + diff

        # print(train_index, test_index)

        train_samples += [[pcd_path, label_path, class_label] 
                          for pcd_path, label_path in class_dict[class_label][: train_index]]
        
        test_samples += [[pcd_path, label_path, class_label] 
                          for pcd_path, label_path in class_dict[class_label][train_index: test_index]]
        
        val_samples += [[pcd_path, label_path,class_label] 
                          for pcd_path, label_path in class_dict[class_label][test_index:]]
        
    
    return train_samples, test_samples, val_samples


def get_raw_point_clouds(path):
    res = []
    files = sorted(os.listdir(path), key = lambda x: int(x))
    for folder in files:
        curr_path = os.path.join(path, folder)
        for pcd in sorted(os.listdir(curr_path)):
            #print(pcd)
            if pcd.endswith('.pcd'):
                complete_path = os.path.join(curr_path, pcd)
                res.append(complete_path)
    return sorted(res, key = lambda x: x.split('_')[-1])

def parse_label_color(path):
    with open(path, "r+") as f:
        lines = f.readlines()
    file_content = "".join(lines).replace("\n", "")
    
    with open("temp_label0003884.py", "w+") as f:
        f.write(file_content)
    
    from temp_label0003884 import global_ids
    os.remove("temp_label0003884.py")
    return global_ids
    
def paint_point_cloud(points, target, color_dict):
    color_array = []
    # print("color_dict = ", color_dict)
    # print(target.shape)
    for i in target:
        # print(int(i))
        color_array.append(color_dict[int(i)])
    pcd = o3d.geometry.PointCloud()
    pcd.colors = o3d.utility.Vector3dVector(color_array)
    pcd.points = o3d.utility.Vector3dVector(points)

    return pcd

####################################
### Main DataLoader class
####################################

class PCDatasetPartSeg(Dataset):
    def __init__(self,split = "train", path = "./dataloader_output",
                normals = False, 
                ):
        self.pcds = []
        self.point_labels = []
        self.labels = []
        self.normals = normals

        self.csv_path = os.path.join(path, "dataset_{}_part_seg.csv".format(split))

        with open(self.csv_path, mode ='r')as file:
            csvreader = csv.reader(file)
            for line in csvreader:
                self.pcds.append(line[0])
                self.point_labels.append(line[1])
                self.labels.append(line[2])

    def __len__(self):
        return len(self.pcds)

    def __getitem__(self, index):
        pcd = o3d.io.read_point_cloud(self.pcds[index])
        point_label = np.load(self.point_labels[index])
        if self.normals:
            pcd.estimate_normals()
            pcd = np.concatenate( (np.asarray(pcd.points),
                                np.asarray(pcd.normals)), axis = 1 )
        else:
            pcd = np.asarray(pcd.points)

        # print(self.pcds[index], self.labels[index])
        return (torch.from_numpy(pcd).to(torch.float32),
                torch.from_numpy(point_label).to(torch.float32),
                torch.tensor([int(self.labels[index]) - 1 ]))

####################################
### Driver Function(s)
####################################

def custom_collate_func(batch):
    random_points_size = np.linspace(80_000, 100_000, 5, dtype = int)
    point_clouds=  [i[0] for i in batch]
    point_labels = [i[1] for i in batch]
    labels = torch.tensor([i[2] for i in batch])
    downsample_size= random.choice(random_points_size)

    downsampled_pcds, downsampled_point_labels = [], []

    for pcd, point_label, label in zip(point_clouds, point_labels, labels):
        downsampled_pcd, downsampled_point_label = downsample_tensor(pcd, point_label, downsample_size, label)
        downsampled_point_labels.append(downsampled_point_label.tolist())
        downsampled_pcds.append(downsampled_pcd.tolist())
    # point_clouds = torch.tensor([downsample_tensor(i, downsample_size).tolist()
    #                               for i in point_clouds])
    downsampled_pcds = torch.tensor(downsampled_pcds)
    downsampled_point_labels = torch.tensor(downsampled_point_labels)
    # print("In dataloader function: ",downsampled_pcds.shape, downsampled_point_label.shape)
    return [downsampled_pcds, downsampled_point_labels, labels]

def generate_csv(path, save_path):

    '''
    dataset: is now folder containing point clouds and labels
    save_path: dataloader_output
    '''

    no_outliers_dates = ["2021-12-21" ,"2021-12-07"] 

    mappings = get_mappings()
    pcd_folders = os.path.join(path, "pointclouds")
    label_folders = os.path.join(path, "labels")
    class_dict = {}
    dates = sorted(os.listdir(pcd_folders))
    for date in dates:
        complete_path_pcd = os.path.join(pcd_folders, date, "pointcloud", "no_outliers" if date in no_outliers_dates else "no_outliers_x2")
        complete_path_label = os.path.join(label_folders, date) 
        # variation_dirs = [ i for i in os.listdir(complete_path) if 'label' not in i]
        # print(date)
        # for vd in variation_dirs:
        # curr_path = os.path.join(complete_path, vd)
        # if len(sorted(os.listdir(complete_path_pcd))) < 48:
        #     pcds = get_raw_point_clouds(curr_path)
        # else:
        pcds = [i for i in sorted(os.listdir(complete_path_pcd)) if i.endswith('.pcd')]
        labels = [i for i in sorted(os.listdir(complete_path_label))]
        
        for idx, (pcd, label) in enumerate(zip(pcds, labels)):
            if idx+1 not in class_dict:
                class_dict[idx+1] = []
            class_dict[mappings[idx+1]].append([os.path.join(complete_path_pcd, pcd), os.path.join(complete_path_label, label)])

    
    train_samples, test_samples, val_samples = split_data(class_dict)
    write_csv_file(os.path.join(save_path, "dataset_train_part_seg.csv"), train_samples)
    write_csv_file(os.path.join(save_path, "dataset_test_part_seg.csv"), test_samples)
    write_csv_file(os.path.join(save_path, "dataset_val_part_seg.csv"), val_samples)


####################################
### For Debugging
####################################
# if __name__ == "__main__":

# #     if not os.path.exists(output_path):
# #         os.mkdir(output_path)
#     # dataset_path = "../thesis_dataset"
#     # output_path = "../dataloader_output"
#     # # mappings = get_mappings()
#     # generate_csv(dataset_path, output_path)

#     # print("Done generating csvs")
    # dataset = PCDatasetPartSeg(split = "val", path = "../dataloader_output")
    # dataloader =  torch.utils.data.DataLoader(dataset,
    #                                         collate_fn=custom_collate_func,
    #                                         batch_size=2,
    #                                         shuffle=True)
    
    # dataiter = iter(dataloader)
    # pcd, point_label,  labels = next(dataiter)
    # print(pcd, point_label, torch.unique(point_label[0]), labels)
#     # print(pcd[0].shape, point_label[0].shape, labels)

#     color_file=  "../thesis_dataset/labels/labels_dict.txt"

#     parse_label_color = parse_label_color(color_file)

#     color_dict = {}
#     for i in parse_label_color:
#         label, _, color = parse_label_color[i]
#         color_dict[label] = color 

#     pcd = paint_point_cloud(pcd[0].numpy(), point_label[0].numpy(), color_dict)
#     o3d.io.write_point_cloud("colored_sample.pcd", pcd)

# #     pcd = pcd.transpose(2, 1)
# #     print(pcd.shape)


#python3 ./data_utils/PCDataLoader.py --dataset ./thesis_dataset/pointclouds --output ./dataloader_output