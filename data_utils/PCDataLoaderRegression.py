

import os
import torch
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset
import open3d as o3d
import csv
import torch
import open3d as o3d
import pandas as pd
import random
import warnings
import json
import argparse
warnings.filterwarnings("ignore")
import random
plt.ion() 

##################################
## Argument Parser
##################################

# parser = argparse.ArgumentParser()
# parser.add_argument('--dataset',
#                     help='''Directory containing Point Clouds''')
# parser.add_argument('--output',
#                     help='''Where CSV file and splitting indices are saved''')

# args = parser.parse_args()

# if not args.dataset or not args.output:
#     print("One of the attribute is missing")
#     exit(0)

# dataset_path = args.dataset
# output_path = args.output

####################################
### Helper Functions
####################################

def downsample_tensor(pcd ,num_pts):
    indices = torch.randperm(pcd.shape[0])[:num_pts]
    pcd = torch.index_select(pcd, 0, indices)
    return pcd
    
def get_mappings():
    mappings = {}
    for i in range(1,13):
        mappings[i] = i
    tags = [11,4,1,9,7,5,10,3,2,6,8,
            12,7,4,2,1,5,9,11,3,10,12,
            8,6,5,8,7,6,1,12,2,9,11,3,10,4]

    for i in range(len(tags)):
        mappings[i+13] = tags[i] 
    return mappings

def write_csv_file(save_path, data):
    with open(save_path, 'w+', newline = '') as file:
        writer = csv.writer(file)
        for row in data: writer.writerow(row)

def get_data_from_csv(path):
    data = []
    with open(path, "r") as file:
        csvreader = csv.reader(file)
        for line in csvreader:
            data.append(line)

def split_data(class_dict, test_ratio = 0.2, val_ratio = 0.2):

    train_samples = []
    test_samples = []
    val_samples = []

    for class_label in class_dict:
        random.shuffle(class_dict[class_label]) 
        n = len(class_dict[1])
        train_index = int(n * (1.0-test_ratio-val_ratio)) 
        test_index = train_index + int(n*test_ratio)

        train_samples += [[pcd_path, class_label] 
                          for pcd_path in class_dict[class_label][: train_index]]
        
        test_samples += [[pcd_path, class_label] 
                          for pcd_path in class_dict[class_label][train_index: test_index]]
        
        val_samples += [[pcd_path, class_label] 
                          for pcd_path in class_dict[class_label][test_index:]]
        
    
    return train_samples, test_samples, val_samples


def get_raw_point_clouds(path):
    res = []
    files = sorted(os.listdir(path), key = lambda x: int(x))
    for folder in files:
        curr_path = os.path.join(path, folder)
        for pcd in sorted(os.listdir(curr_path)):
            #print(pcd)
            if pcd.endswith('.pcd'):
                complete_path = os.path.join(curr_path, pcd)
                res.append(complete_path)
    return sorted(res, key = lambda x: x.split('_')[-1])

####################################
### Main DataLoader class
####################################

class PCDatasetRegression(Dataset):
    def __init__(self,split = "train", path = "./dataloader_output",
                normals = False, reg_params = 4 
                ):
        self.pcds = []
        self.labels = []
        self.normals = normals

        if split == "downsample_test":
            print("Testing model on already downsampled PCD's")
            self.csv_path = os.path.join(path, "normalized_cropped_pcd_params.csv")
        else:
            self.csv_path = os.path.join(path, "dataset_{}_regression.csv".format(
                        split))

        with open(self.csv_path, mode ='r') as file:
            csvreader = csv.reader(file)
            for line in csvreader:
                # self.pcds.append("." + line[0])
                self.pcds.append(line[0])
                params = []
                for i in range(reg_params):
                    params.append(float(line[2+i]))
                # print(params)
                self.labels.append(params)

    def __len__(self):
        return len(self.pcds)

    def __getitem__(self, index):
        pcd = o3d.io.read_point_cloud(self.pcds[index])
        if self.normals:
            pcd.estimate_normals()
            pcd = np.concatenate( (np.asarray(pcd.points),
                                np.asarray(pcd.normals)), axis = 1 )
        else:
            pcd = np.asarray(pcd.points)
        return (torch.from_numpy(pcd).to(torch.float32), 
                torch.tensor(self.labels[index]))

####################################
### Driver Function(s)
####################################

def custom_collate_func(batch):
    random_points_size = np.linspace(80_000, 100_000, 5, dtype = int)
    point_clouds=  [i[0] for i in batch]
    # print(i[1])
    labels = torch.tensor([i[1].numpy() for i in batch])
    downsample_size= random.choice(random_points_size) 
    point_clouds = torch.tensor([downsample_tensor(i, downsample_size).tolist()
                                  for i in point_clouds])

    return [point_clouds, labels]

def generate_csv(path, save_path):
    '''
    path: CSV file with regression params
    save_path: directory containing dataset_{}.csv.format([train, test, val]) files
    '''
    reg_params = pd.read_csv(path)

    attr_dict = {}

    for col in reg_params.columns:
        if reg_params[col].dtype == "float64":
            attr_dict[col] = {}
            attr_dict[col]["mean"] = reg_params[col].mean()
            attr_dict[col]["std"] = reg_params[col].std()
            
            reg_params[col] = (reg_params[col]-reg_params[col].mean())/reg_params[col].std()


    reg_params.to_csv('{}/normalized_regression_values.csv'.format(save_path),
                        header = False,
                        index = False)
    
    json_object = json.dumps(attr_dict, indent=4)
    with open("{}/attr.json".format(save_path), "w") as outfile:
        outfile.write(json_object)



    ##############################################################
    ## Some helper functions for dataframes
    ##############################################################
    def get_id(name):
        return int(name.split("/")[-1].split("_")[-1].split(".")[0])

    def get_date(name):
        return name.split("/")[-1].split("_")[0]


    def get_height_value(name):
        
        # print(get_id(name), reg_params[reg_params["date"] == get_date(name)].iloc[41]) 
        return reg_params[reg_params["date"] == get_date(name)].iloc[get_id(name) - 1]["height"]

    def get_width_value(name):
        return reg_params[reg_params["date"] == get_date(name)].iloc[get_id(name) - 1]["width"]

    def get_conv_area_value(name):
        return reg_params[reg_params["date"] == get_date(name)].iloc[get_id(name) - 1]["conv_area"]

    def get_conv_volume_value(name):
        return reg_params[reg_params["date"] == get_date(name)].iloc[get_id(name) - 1]["conv_volume"]


    dataset_csvs = ["train", "test", "val"]

    for csv_file in dataset_csvs:
        samples = pd.read_csv("{}/dataset_{}.csv".format( save_path, 
                    csv_file), names = ["path", "label"])
        # test_sample = samples.iloc[1] 
        # print(get_id(test_sample['path']), get_date(test_sample['path'] ))
        samples["height"] = samples["path"].apply(get_height_value)
        # print('--------------------------------------------here')
        samples["width"] = samples["path"].apply(get_width_value)
        samples["conv_area"] = samples["path"].apply(get_conv_area_value)
        samples["conv_value"] = samples["path"].apply(get_conv_volume_value)
        
        samples.to_csv('{}/dataset_{}_regression.csv'.format(save_path, csv_file),
                        header = False,
                        index = False)


####################################
### For Debugging
####################################
# if __name__ == "__main__":
#     generate_csv("/home/sensorik/Documents/thesis_h/repo_code/dataloader_output/regression_parameters.csv", 
#                  '/home/sensorik/Documents/thesis_h/repo_code/dataloader_output')

#     trainloader = PCDatasetRegression(split = "val",
#                     path="/home/sensorik/Documents/thesis_h/repo_code/dataloader_output"
#     )

#     pcd, data  = next(iter(trainloader))

#     print(data)

#python3 ./data_utils/PCDataLoader.py --dataset ./thesis_dataset/pointclouds --output ./dataloader_output
