import os
import torch
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset
import open3d as o3d
import csv
import torch
import open3d as o3d
import random
import warnings
import argparse
warnings.filterwarnings("ignore")
import random
plt.ion() 

##################################
## Argument Parser
##################################

# parser = argparse.ArgumentParser()
# parser.add_argument('--dataset',
#                     help='''Directory containing Point Clouds''')
# parser.add_argument('--output',
#                     help='''Where CSV file and splitting indices are saved''')

# args = parser.parse_args()

# if not args.dataset or not args.output:
#     print("One of the attribute is missing")
#     exit(0)

# dataset_path = args.dataset
# output_path = args.output

####################################
### Helper Functions
####################################

def downsample_tensor(pcd ,num_pts):
    indices = torch.randperm(pcd.shape[0])[:num_pts]
    pcd = torch.index_select(pcd, 0, indices)
    return pcd
    
def get_mappings():
    mappings = {}
    for i in range(1,13):
        mappings[i] = i
    tags = [11,4,1,9,7,5,10,3,2,6,8,
            12,7,4,2,1,5,9,11,3,10,12,
            8,6,5,8,7,6,1,12,2,9,11,3,10,4]

    for i in range(len(tags)):
        mappings[i+13] = tags[i] 
    return mappings

def write_csv_file(save_path, data):
    with open(save_path, 'w+', newline = '') as file:
        writer = csv.writer(file)
        for row in data: writer.writerow(row)

def get_data_from_csv(path):
    data = []
    with open(path, "r") as file:
        csvreader = csv.reader(file)
        for line in csvreader:
            data.append(line)

def split_data(class_dict, test_ratio = 0.2, val_ratio = 0.2):

    train_samples = []
    test_samples = []
    val_samples = []

    for class_label in class_dict:
        random.shuffle(class_dict[class_label]) 
        n = len(class_dict[1])
        train_index = int(n * (1.0-test_ratio-val_ratio)) 
        test_index = train_index + int(n*test_ratio)

        train_samples += [[pcd_path, class_label] 
                          for pcd_path in class_dict[class_label][: train_index]]
        
        test_samples += [[pcd_path, class_label] 
                          for pcd_path in class_dict[class_label][train_index: test_index]]
        
        val_samples += [[pcd_path, class_label] 
                          for pcd_path in class_dict[class_label][test_index:]]
        
    
    return train_samples, test_samples, val_samples


def get_raw_point_clouds(path):
    res = []
    files = sorted(os.listdir(path), key = lambda x: int(x))
    for folder in files:
        curr_path = os.path.join(path, folder)
        for pcd in sorted(os.listdir(curr_path)):
            #print(pcd)
            if pcd.endswith('.pcd'):
                complete_path = os.path.join(curr_path, pcd)
                res.append(complete_path)
    return sorted(res, key = lambda x: x.split('_')[-1])

####################################
### Main DataLoader class
####################################

class PCDataset(Dataset):
    def __init__(self,split = "train", path = "./dataloader_output",
                normals = False, 
                ):
        self.pcds = []
        self.labels = []
        self.normals = normals

        self.csv_path = os.path.join(path, "dataset_{}.csv".format(split))

        with open(self.csv_path, mode ='r')as file:
            csvreader = csv.reader(file)
            for line in csvreader:
                self.pcds.append(line[0])
                self.labels.append(line[1])

    def __len__(self):
        return len(self.pcds)

    def __getitem__(self, index):
        pcd = o3d.io.read_point_cloud(self.pcds[index])
        if self.normals:
            pcd.estimate_normals()
            pcd = np.concatenate( (np.asarray(pcd.points),
                                np.asarray(pcd.normals)), axis = 1 )
        else:
            pcd = np.asarray(pcd.points)
        return (torch.from_numpy(pcd).to(torch.float32), 
                torch.tensor([int(self.labels[index]) - 1 ]))

####################################
### Driver Function(s)
####################################

def custom_collate_func(batch):
    random_points_size = np.linspace(80_000, 100_000, 5, dtype = int)
    point_clouds=  [i[0] for i in batch]
    labels = torch.tensor([i[1] for i in batch])
    downsample_size= random.choice(random_points_size) 
    point_clouds = torch.tensor([downsample_tensor(i, downsample_size).tolist()
                                  for i in point_clouds])

    return [point_clouds, labels]

def generate_csv(path, save_path):
    mappings = get_mappings()
    dates = os.listdir(path)
    class_dict = {}
    for date in dates:
        complete_path = os.path.join(path, date, "pointcloud")
        variation_dirs = [ i for i in os.listdir(complete_path) if 'label' not in i]
        print(date)
        for vd in variation_dirs:
            curr_path = os.path.join(complete_path, vd)
            if len(sorted(os.listdir(curr_path))) < 48:
                pcds = get_raw_point_clouds(curr_path)
            else:
                pcds = [i for i in sorted(os.listdir(curr_path)) if i.endswith('.pcd')]
            
            for idx, pcd in enumerate(pcds):
                if idx+1 not in class_dict:
                    class_dict[idx+1] = []
                class_dict[mappings[idx+1]].append(os.path.join(curr_path, pcd))

    
    train_samples, test_samples, val_samples = split_data(class_dict)
    write_csv_file(os.path.join(save_path, "dataset_train.csv"), train_samples)
    write_csv_file(os.path.join(save_path, "dataset_test.csv"), test_samples)
    write_csv_file(os.path.join(save_path, "dataset_val.csv"), val_samples)


####################################
### For Debugging
####################################
# if __name__ == "__main__":

#     if not os.path.exists(output_path):
#         os.mkdir(output_path)

#     mappings = get_mappings()
#     generate_csv(dataset_path, output_path)
#     dataset = PCDataset(split = "val")
#     dataloader =  torch.utils.data.DataLoader(dataset,
#                                             collate_fn=custom_collate_func,
#                                             batch_size=2,
#                                             shuffle=True)
    
#     dataiter = iter(dataloader)
#     pcd, labels = next(dataiter)
#     pcd = pcd.transpose(2, 1)
#     print(pcd.shape)


#python3 ./data_utils/PCDataLoader.py --dataset ./thesis_dataset/pointclouds --output ./dataloader_output