import matplotlib.pyplot as plt
import numpy as np
import os
import re
import argparse

##################################
## Argument Parser
##################################

parser = argparse.ArgumentParser()
parser.add_argument('--log_dir',
                    help='''Directory containing log file''')

args = parser.parse_args()

if not args.log_dir:
    print("One of the attribute is missing")
    exit(0)

logs = args.log_dir

##################################
## Helper Function(s)
##################################
def split_data(data):
    data = re.findall(r"[\w\.']+", data)
    return [float(data[i+1]) 
            for i in range(len(data))
            if "Accuracy" in data[i]]

##################################
## Driver Program
##################################

if __name__ == "__main__":
    complete_path = os.path.join("log/classification",
                                 logs,
                                 "logs")
    
    complete_path = os.path.join(complete_path,
                                 os.listdir(complete_path)[0])
    
    with open(complete_path, "r+") as f:
        file_content = f.read().split("\n")

    # Parsing Log File
    train_acc = []
    test_acc = []
    # class_acc = []
    for line in file_content:
        for data in line.split("-"):
            if "Train Instance" in data:
                train_acc.append(split_data(data)[0])
            if "Test Instance" in data:
                res = split_data(data)
                test_acc.append(res[0])
                # class_acc.append(res[1])

    # Plotting Result

    x_legend_size = min(len(train_acc), len(test_acc))
    offset = 0.02
    plt.plot(list(range(x_legend_size)), train_acc[:x_legend_size], label = "Training Accuracy")
    plt.plot(list(range(x_legend_size)), test_acc[:x_legend_size], label = "Validation Accuracy")
    train_min_index = train_acc.index(max(train_acc[:x_legend_size]))
    train_min_value = train_acc[train_min_index]
    plt.scatter([train_min_index], [train_min_value])
    plt.text(train_min_index + offset,
              train_min_value + offset,
            "({}, {})".format(train_min_index, train_min_value))
    
    test_min_index = test_acc.index(max(test_acc[:x_legend_size]))
    test_min_value = test_acc[test_min_index]
    plt.scatter([test_min_index], [test_min_value])
    plt.text(test_min_index + offset,
              test_min_value + offset,
            "({}, {})".format(test_min_index, test_min_value))
    plt.legend()
    plt.title("Accuracy Plot")
    plt.xlabel("Epoch")
    plt.ylabel("Accuracy")
    plt.show()